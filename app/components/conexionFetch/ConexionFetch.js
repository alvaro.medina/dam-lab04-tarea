import React, { Component } from 'react';
import {View, FlatList, StyleSheet, Text, Image,TouchableOpacity} from 'react-native';



export default class ConexionFetch extends Component{
    constructor(props){
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
        };
    }
    async componentDidMount() {
        await fetch('https://yts.mx/api/v2/list_movies.json')
            .then(res => res.json())
            .then(
                result => {
                    console.warn('result', result.data.movies);
                    this.setState({
                        items: result.data.movies,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={this.state.items.length > 0 ? this.state.items : []}
                    renderItem={({item}) => (
                        <DATA
                            id={item.id}  
                            title={item.title} 
                            image={item.medium_cover_image} 
                            description_full={item.description_full}
                            navigation={this.props.navigation}
                        />
                    )}
                    keyExtractor={DATA => DATA.id}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#7986CB',
    },
    itemContainer:{
        backgroundColor:'#BBDEFB',
        padding:20,
        marginVertical:8,
        marginHorizontal:16,
    },
    title:{
        fontSize:35,
    },
});






function DATA({id, title, image, description_full, navigation}) {
    return(
        <TouchableOpacity  
            style={{borderBottomWidth: 1}}
            onPress={() => navigation.navigate('Details',{
                itemId: id,
            })}
            >
          <View style={styles.itemContainer}>
            <View>
              <Image source={{uri:image}} style={{height:80, width:80}}></Image>
            </View>
            <View>
                <Text style={styles.title}>{title}</Text>
                <Text numberOfLines={3} 
                    style={styles.itemSubtitle}>{description_full}</Text>
            </View>

          </View>
        </TouchableOpacity>
    );
}
